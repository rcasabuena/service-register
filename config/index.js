const bunyan = require("bunyan");
const pkgjs = require("../package.json");
const { name, version } = pkgjs;

const getLogger = (name, version, level) =>
  bunyan.createLogger({ name: `${name}:${version}`, level });

module.exports = {
  development: {
    name,
    version,
    log: () => getLogger(name, version, "debug"),
  },
  production: {
    name,
    version,
    log: () => getLogger(name, version, "info"),
  },
};
