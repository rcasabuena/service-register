const { body } = require("express-validator");

const serviceValidator = [
  body("name")
    .trim()
    .not()
    .isEmpty()
    .withMessage("name is required")
    .isString()
    .withMessage("name must be a string")
    .escape(),
  body("version")
    .trim()
    .not()
    .isEmpty()
    .withMessage("version is required")
    .escape(),
];

const serviceValidatorWithPort = [
  ...serviceValidator,
  body("version")
    .isSemVer()
    .withMessage("version must be in semantic versioning format")
    .escape(),
  body("port")
    .trim()
    .not()
    .isEmpty()
    .withMessage("port is required")
    .isPort()
    .withMessage("Must be a valid port number")
    .escape(),
];

const serviceErrorFormatter = (err) => {
  let errors = {};
  err.forEach((error) => {
    if (undefined === errors[error.param]) errors[error.param] = [];
    errors[error.param].push(error.msg);
  });
  return { error: { errors } };
};

module.exports = {
  serviceValidator,
  serviceValidatorWithPort,
  serviceErrorFormatter,
};
