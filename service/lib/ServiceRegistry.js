const semver = require("semver");

class ServiceRegistry {
  constructor({ client, log }) {
    if (!client) throw new Error("Missing db client");
    this.log = log;
    this.client = client;
    this.timeout = 30;
  }

  async get(service) {
    await this.cleanup();
    try {
      const services = await this.client.hgetallAsync(`service`);
      if (services) {
        const candidates = Object.values(services).filter((s) => {
          s = JSON.parse(s);
          return (
            s.name === service.name &&
            semver.satisfies(s.version, service.version)
          );
        });
        if (candidates.length > 0) {
          return JSON.parse(
            candidates[Math.floor(Math.random() * candidates.length)]
          );
        }
      }
      return null;
    } catch (err) {
      throw new Error("Error when finding service");
    }
  }

  async register(service) {
    await this.cleanup();
    try {
      await this.client.hsetAsync(`service`, service.key, service.stringify());
      this.log.debug(
        `Registered/updated services ${service.name}, version ${service.version} at ${service.ip}:${service.port}`
      );
      return service.key;
    } catch (err) {
      throw new Error("Error when registering service");
    }
  }

  async unregister(service) {
    try {
      await this.client.hdelAsync(`service`, service.key);
      this.log.debug(
        `Unregistered service ${service.name}, version ${service.version} at ${service.ip}:${service.port}`
      );
      return service.key;
    } catch (err) {
      throw new Error("Error when unregistering service");
    }
  }

  async cleanup() {
    try {
      const services = await this.client.hgetallAsync(`service`);
      if (services) {
        const now = Math.floor(new Date() / 1000);
        Object.keys(services).forEach(async (key) => {
          const service = JSON.parse(services[key]);
          if (service.timestamp + this.timeout < now) {
            try {
              await this.client.hdelAsync(`service`, service.key);
              this.log.debug(`Removed service ${service.key}`);
            } catch (err) {
              throw new Error("Error during services cleanup");
            }
          }
        });
      }
    } catch (err) {
      throw new Error("Error during service cleanup");
    }
  }
}

module.exports = ServiceRegistry;
