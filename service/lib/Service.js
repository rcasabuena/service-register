class Service {
  constructor({ name, version, ip, port }, partial = false) {
    this.name = name;
    this.version = version;

    if (!partial) {
      this.ip = ip;
      this.port = port;
      this.timestamp = Math.floor(new Date() / 1000);
      this.key = name + version + ip + port;
    }
  }

  stringify() {
    return JSON.stringify(this);
  }
}

module.exports = Service;
