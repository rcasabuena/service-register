const express = require("express");
const { validationResult } = require("express-validator");
const {
  serviceValidator,
  serviceValidatorWithPort,
  serviceErrorFormatter,
} = require("./lib/ServiceValidator");
const Service = require("./lib/Service");
const ServiceRegistry = require("./lib/ServiceRegistry");

const service = express();

module.exports = (config) => (redisClient) => {
  const log = config.log();
  const serviceRegistry = new ServiceRegistry({ client: redisClient, log });

  service.use(express.json());

  /* Check is request is authorised */
  service.use((req, res, next) => {
    const srk = req.header("Service-Registration-Key");
    if (
      !srk ||
      !process.env.REGISTRATION_KEY ||
      srk !== process.env.REGISTRATION_KEY
    )
      res.status(401).json({ error: "Unauthorised" });
    next();
  });

  service.put("/register", serviceValidatorWithPort, async (req, res, next) => {
    const results = validationResult(req);
    if (results.errors.length > 0) {
      res.status(400).json(serviceErrorFormatter(results.errors));
    } else {
      const { name, version, port } = req.body;
      const ip = req.connection.remoteAddress.includes("::")
        ? `[${req.connection.remoteAddress}]`
        : req.connection.remoteAddress;

      const service = new Service({ name, version, ip, port });

      try {
        const key = await serviceRegistry.register(service);
        res.status(200).json({ result: key });
      } catch (err) {
        console.log(err.message);
        res.status(400).json({ error: err.message });
      }
    }
  });

  service.delete(
    "/register",
    serviceValidatorWithPort,
    async (req, res, next) => {
      const results = validationResult(req);
      if (results.errors.length > 0) {
        res.status(400).json(serviceErrorFormatter(results.errors));
      } else {
        const { name, version, port } = req.body;
        const ip = req.connection.remoteAddress.includes("::")
          ? `[${req.connection.remoteAddress}]`
          : req.connection.remoteAddress;

        const service = new Service({ name, version, ip, port });

        try {
          const key = await serviceRegistry.unregister(service);
          res.status(200).json({ result: key });
        } catch (err) {
          res.status(400).json({ error: err.message });
        }
      }
    }
  );

  service.get("/find", serviceValidator, async (req, res, next) => {
    const results = validationResult(req);
    if (results.errors.length > 0) {
      res.status(400).json(serviceErrorFormatter(results.errors));
    } else {
      const { name, version } = req.body;
      const service = new Service({ name, version }, true);
      try {
        const svc = await serviceRegistry.get(service);
        if (!svc) res.status(404).json({ error: "Service not found" });
        else res.status(200).json(svc);
      } catch (err) {
        res.status(400).json({ error: err.message });
      }
    }
  });

  /* Handle endpoints that are not implemented */
  service.use("*", function (req, res) {
    res.status(405).json({ error: "Method not allowed" });
  });

  /* Default error handler */
  service.use((error, req, res, next) => {
    if (error) res.status(error.status || 500).json({ error: error.message });
    next();
  });

  return service;
};
